<?php
declare(strict_types=1);

namespace Kowal\EksportFaktur\Controller\Adminhtml\Invoice;

use Kowal\EksportFaktur\Model\LegacyEncoder;
use Kowal\EksportFaktur\Service\Csv;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Api\Data\InvoiceInterface;
use Magento\Sales\Api\InvoiceRepositoryInterface;
use Magento\Sales\Model\ResourceModel\Order\Invoice\CollectionFactory;
use Magento\Ui\Component\MassAction\Filter;
use Psr\Log\LoggerInterface;

class EksportFakturCsv implements HttpPostActionInterface
{
    /**
     * @param RequestInterface $request
     * @param ResultFactory $resultFactory
     * @param InvoiceRepositoryInterface $invoiceRepository
     * @param SearchCriteriaBuilder $criteriaBuilder
     * @param Csv $csvService
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param LoggerInterface $logger
     * @param LegacyEncoder $legacyEncoder
     */
    public function __construct(
        private readonly RequestInterface $request,
        private readonly ResultFactory $resultFactory,
        private readonly InvoiceRepositoryInterface $invoiceRepository,
        private readonly SearchCriteriaBuilder $criteriaBuilder,
        private readonly Csv $csvService,
        private readonly Filter $filter,
        private readonly CollectionFactory $collectionFactory,
        private readonly LoggerInterface $logger,
        private readonly LegacyEncoder $legacyEncoder
    ) {
    }

    /**
     * Execute
     *
     * @return ResponseInterface|Redirect|ResultInterface
     */
    public function execute()
    {
        $selectedIds = $this->request->getParam('selected', []);
        $dateRange = $this->getDateRange($this->request->getParam('filters')['created_at'] ?? null);


        if (empty($selectedIds)) {
            try {
                $collection = $this->filter->getCollection($this->collectionFactory->create());
                $selectedIds = $collection->getAllIds();
                if (empty($selectedIds)) {
                    return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)->setPath('sales/invoice');
                }
            } catch (LocalizedException $e) {
                $this->logger->error($e->getMessage());
            }
        }

        $criteria = $this->criteriaBuilder
            ->addFilter(InvoiceInterface::ENTITY_ID, $selectedIds, 'in')
            ->create();

        $invoiceList = $this->invoiceRepository->getList($criteria);

        $data = $this->csvService->getData($invoiceList);
        $content = mb_convert_encoding($data->getBody(), 'ISO-8859-2');
        $legacyEncodedContent = $this->legacyEncoder->process($content);
        $this->legacyEncoder->removeCsv(LegacyEncoder::ENCODED_FILE_NAME);

        $result = $this->resultFactory->create(ResultFactory::TYPE_RAW);
        $this->setResponseHeaders($result, $dateRange);
        $result->setContents($legacyEncodedContent);

        return $result;
    }

    /**
     * Get selected date range
     *
     * @param array|null $dateRange
     * @return string|null
     */
    private function getDateRange(?array $dateRange): ?string
    {
        if (isset($dateRange['from']) && isset($dateRange['to'])) {
            return $dateRange['from'] . '-' . $dateRange['to'];
        }
        return null;
    }

    /**
     * Set response headers
     *
     * @param ResultInterface $result
     * @param string|null $dateRange
     * @return void
     */
    private function setResponseHeaders(ResultInterface $result, ?string $dateRange): void
    {
        $result->setHeader('Content-Type', 'text/csv');

        $filename = 'eksport_faktur';
        if ($dateRange) {
            $filename .= '(' . $dateRange . ')';
        }
        $filename .= '.csv';

        $result->setHeader('Content-Disposition', 'attachment; filename=' . $filename);
        $result->setHeader('Pragma', 'no-cache');
    }
}
