<?php
declare(strict_types=1);

namespace Kowal\EksportFaktur\Enum;

enum LegacyFormats: string
{
    case ISO_LATIN_1 = '4';
    case MAZOVIA = '1';

    /**
     * Get format symbol
     *
     * @return string
     */
    public function formatSymbol(): string
    {
        return match($this)
        {
            LegacyFormats::ISO_LATIN_1 => '4',
            LegacyFormats::MAZOVIA => '1'
        };
    }
}
