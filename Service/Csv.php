<?php
declare(strict_types=1);

namespace Kowal\EksportFaktur\Service;

use Kowal\EksportFaktur\Service\CsvFields;
use Kowal\EksportFaktur\Service\CountriesTax;
use Magento\Sales\Api\Data\InvoiceSearchResultInterface;
use Magento\Sales\Api\Data\CreditmemoSearchResultInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Api\Data\TransactionSearchResultInterfaceFactory;
use Magento\Sales\Model\ResourceModel\Order\Shipment\CollectionFactory as ShipmentCollectionFactory;

class Csv
{
    /**
     * @var array
     */
    private array $data = [];

    /**
     * @param OrderRepositoryInterface $orderRepository
     * @param \Kowal\EksportFaktur\Service\CountriesTax $countriesTax
     * @param TransactionSearchResultInterfaceFactory $transactionSearchResultInterfaceFactory
     * @param SearchResultFactory $searchResultFactory
     * @param CollectionProcessorInterface|null $collectionProcessor
     */
    public function __construct(
        private readonly OrderRepositoryInterface       $orderRepository,
        private CountriesTax                            $countriesTax,
        private TransactionSearchResultInterfaceFactory $transactionSearchResultInterfaceFactory,
        private ShipmentCollectionFactory               $shipmentCollectionFactory

    )
    {

        $this->countriesTax = $countriesTax;
        $this->transactionSearchResultInterfaceFactory = $transactionSearchResultInterfaceFactory;
        $this->shipmentCollectionFactory = $shipmentCollectionFactory;
    }



    /**
     * Csv data
     *
     * @param CreditmemoSearchResultInterface $invoices
     * @return $this
     */
    public function getCreditData(CreditmemoSearchResultInterface $creditmemos): self
    {
        $this->creditData = [];
        $this->creditData[] = [
            'Data korekty',
            'Nr korekty',
            'Kontrahent',
            'Wartosc Brutto',
            'Waluta',
            'Stawka VAT',
            'Netto',
            'VAT',
            'Rodzaj Platnosci',
            'Kraj Odbioru',
            'ID platnosci',
            'Marketplace',
            'Nr zamowienia',
            'Kurs NBP'
        ];

        $taxType = "standard";

        foreach ($creditmemos as $creditmemo) {
            $order = $this->getOrder((int)$creditmemo->getOrderId());
            $billingAddress = $order->getBillingAddress();
            $countryId = is_null($order->getShippingAddress()) ? "" : $order->getShippingAddress()->getCountryId();
            $stawkaVat = (int)$this->countriesTax->getTaxRate($taxType, $countryId);

            $this->creditData[] = [
                $this->formatDate($creditmemo->getCreatedAt()),
                (string)$creditmemo->getIncrementId(),
                $billingAddress->getVatId() ?
                    (string)$billingAddress->getCompany() :
                    (string)$billingAddress->getFirstname() . ' ' . (string)$billingAddress->getLastname(),

                number_format((float)$creditmemo->getGrandTotal(), 2, ",", ""),
                $creditmemo->getOrderCurrencyCode(),
                $stawkaVat,
                number_format((float)$this->getTaxAmount($creditmemo, $stawkaVat), 2, ",", ""),
                number_format((float)$creditmemo->getGrandTotal() - (float)$this->getTaxAmount($creditmemo, $stawkaVat), 2, ",", ""),
                $order->getPayment()->getMethodInstance()->getCode(),
                $countryId,
                $this->getTransactionID($order,'refund'),
                $this->getMarketplace($order),
                $order->getIncrementId(),
                $creditmemo->getKursNbp()
            ];
        }

        return $this;
    }

    /**
     * Csv data
     *
     * @param InvoiceSearchResultInterface $invoices
     * @return $this
     */
    public function getData(InvoiceSearchResultInterface $invoices): self
    {
        $this->data = [];
        $this->data[] = [
            'Data faktury',
            'Nr faktury',
            'Kontrahent',
            'Data wysylki',
            'Wartosc Brutto',
            'Waluta',
            'Stawka VAT',
            'Netto',
            'VAT',
            'Rodzaj Platnosci',
            'Kraj Odbioru',
            'ID platnosci',
            'Zwrot',
            'Marketplace',
            'Nr zamowienia',
            'NIP',
            'Kurs',
            'WZ',
            'Kod rabatowy',
            'Status'
        ];

        $doubleZero = $this->getDoubleZero();
        $taxType = "standard";

        foreach ($invoices as $invoice) {
            $order = $this->getOrder((int)$invoice->getOrderId());
            $billingAddress = $order->getBillingAddress();
            $countryId = is_null($order->getShippingAddress()) ? "" : $order->getShippingAddress()->getCountryId();
            $stawkaVat = (int)$this->countriesTax->getTaxRate($taxType, $countryId);

            $this->data[] = [
                $this->formatDate($invoice->getCreatedAt()),
                (string)$invoice->getIncrementId(),
                $billingAddress->getVatId() ?
                    (string)$billingAddress->getCompany() :
                    (string)$billingAddress->getFirstname() . ' ' . (string)$billingAddress->getLastname(),
                $this->formatDate($this->getShipmentDate($order)),
                number_format((float)$order->getGrandTotal(), 2, ",", ""),
                $invoice->getOrderCurrencyCode(),
                $stawkaVat,
                number_format((float)$this->getTaxAmount($order, $stawkaVat), 2, ",", ""),
                number_format((float)$order->getGrandTotal() - (float)$this->getTaxAmount($order, $stawkaVat), 2, ",", ""),

                $order->getPayment()->getMethodInstance()->getCode(),
                $countryId,
                $this->getTransactionID($order),
                $invoice->getIsUsedForRefund() ? 'Yes' : 'No',
                $this->getMarketplace($order),
                $order->getIncrementId(),
                $billingAddress->getVatId(),
                $order->getKursNbp(),
                $order->getWfirmaWzFullnumber(),
                $order->getCouponCode(),
                $order->getStatus(),
            ];
        }

        return $this;
    }


    private function getTaxAmount($creditmemo, $stawkaVat)
    {
        return (float)$creditmemo->getGrandTotal() / (($stawkaVat + 100) / 100);
    }

    private function getMarketplace($order){
        return explode(PHP_EOL, $order->getStoreName())[0];
    }

    /**
     * @param $order
     * @param $type 'refund' || 'capture'
     * @return mixed|string
     */
    private function getTransactionID($order, $type = 'capture')
    {
        switch ($order->getPayment()->getMethodInstance()->getCode()){
            case 'payu_gateway':
                $transaction = $this->transactionSearchResultInterfaceFactory->create()
                    ->addOrderIdFilter($order->getEntityId())
                    ->addTxnTypeFilter('capture')
                    ->getFirstItem();
                    if($info = $transaction->getData('additional_information') ){
                        if(isset($info['payment_id'])){
                            return $info['payment_id'];
                        }
                    }
                return "";
            case 'przelewy24':
                $transaction = $this->transactionSearchResultInterfaceFactory->create()
                    ->addOrderIdFilter($order->getEntityId())
                    ->addTxnTypeFilter('capture')
                    ->getFirstItem();
                if($info =  $transaction->getData('additional_information') ){

                        if (isset($info['raw_details_info']['statement'])) {
                            return ltrim((string)$info['raw_details_info']['statement'], "p24-");
                        }

                }
                return "";
            case 'checkmo':
                return $order->getBaselinkerTransactionId();
            case 'stripe_payments':
                $transaction = $this->transactionSearchResultInterfaceFactory->create()
                    ->addOrderIdFilter($order->getEntityId())
                    ->addTxnTypeFilter($type)
                    ->getFirstItem();
                if($transaction->getData('txn_id')) {
                    return $transaction->getData('txn_id');
                }else{
                    return "";
                }
            default:
                $transaction = $this->transactionSearchResultInterfaceFactory->create()
                    ->addOrderIdFilter($order->getEntityId())
                    ->addTxnTypeFilter($type)
                    ->getFirstItem();
                return $transaction->getData('txn_id');
        }

    }


    private function getShipmentDate($order)
    {
        if ($shipments = $this->shipmentCollectionFactory->create()->addFieldToFilter('order_id', $order->getEntityId())->getItems()) {
            foreach ($shipments as $shipment) {
                return $shipment->getCreatedAt();
                break;
            }
        } else {
            return $order->getCreatedAt();
        }
    }

    /**
     * Get order
     *
     * @param int $orderId
     * @return OrderInterface
     */
    private function getOrder(int $orderId): OrderInterface
    {
        return $this->orderRepository->get($orderId);
    }

    /**
     * Get formatted date
     *
     * @param string $date
     * @return string
     */
    private function formatDate(string $date): string
    {
        return date('Y-m-d', strtotime($date));
    }

    /**
     * Get double zero
     *
     * @return string
     */
    private function getDoubleZero(): string
    {
        return number_format(0, 2, '.', '');
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody(): string
    {
        $data = '';

        foreach ($this->data as $row) {
            $data .= implode(
                    CsvFields::DELIMITER,
                    array_map(function ($column) {
                        return CsvFields::ENCLOSURE . $column . CsvFields::ENCLOSURE;
                    }, $row)
                ) . PHP_EOL;
        }

        return $data;
    }


    public function getCreditBody(): string
    {
        $data = '';
        foreach ($this->creditData as $row) {
            $data .= implode(
                    CsvFields::DELIMITER,
                    array_map(function ($column) {
                        return CsvFields::ENCLOSURE . $column . CsvFields::ENCLOSURE;
                    }, $row)
                ) . PHP_EOL;
        }

        return $data;
    }
}
