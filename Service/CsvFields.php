<?php

declare(strict_types=1);

namespace Kowal\EksportFaktur\Service;

interface CsvFields
{
    public const VAT_REGISTER_NAME = 'SPRZEDAZ';
    public const COUNTERPARTY_CODE = 'DETAL';
    public const TRANSACTION_DESCRIPTION_CODE = '731';
    public const TRANSACTION_DESCRIPTION = 'Przychody ze sprzedaży towarów';
    public const INVOICE_CORRECTION = 'KOR';
    public const DELIMITER = ',';
    public const ENCLOSURE = '"';
}
