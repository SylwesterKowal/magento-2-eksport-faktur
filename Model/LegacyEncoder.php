<?php
declare(strict_types=1);

namespace Kowal\EksportFaktur\Model;

use Kowal\EksportFaktur\Enum\LegacyFormats;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Driver\File;
use Psr\Log\LoggerInterface;

class LegacyEncoder
{

    public const REPORT_FILE_PATH = 'tmp_report_file.csv';
    public const ENCODED_FILE_NAME = 'encoded_report_file.csv';
    public const ENCODED_FILE_NAME_CREDIR = 'encoded_report_file.csv';
    public const NOT_ENCODED_FILE_NAME = 'tmp_report_file.csv';

    /**
     * @param File $file
     * @param Filesystem $filesystem
     * @param LoggerInterface $logger
     * @param Filesystem\DirectoryList $directoryList
     * @param File $fileDriver
     */
    public function __construct(
        private readonly File                     $file,
        private readonly Filesystem               $filesystem,
        private readonly LoggerInterface          $logger,
        private readonly Filesystem\DirectoryList $directoryList,
        private readonly File                     $fileDriver
    )
    {
    }

    /**
     * Process of converting to legacy encoder
     *
     * @param string $fileContent
     * @return bool|string
     */
    public function process(string $fileContent): bool|string
    {

        $this->tempFileSave($fileContent);
        $this->convertFile();
//        $this->removeCsv(self::NOT_ENCODED_FILE_NAME);
        $encodedFileContent = @file_get_contents($this->getExecDir() . '/' . self::REPORT_FILE_PATH);

        return ($encodedFileContent) ?: false;
    }

    /**
     * Remove csv from server
     *
     * @param string $csvName
     * @return void
     */
    public function removeCsv(string $csvName): void
    {
        try {
            $varTmpDirectory = $this->filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
            $varTmpDirectory->delete($this->getExecDir() . '/' . $csvName);
        } catch (FileSystemException $e) {
            $this->logger->error($e->getMessage());
        }
    }

    /**
     * Save temporary file before encoding
     *
     * @param string $fileContent
     * @return void
     * TODO method writeFile save file as text/plain instead of application/csv- this is obligatory to legacy encoder work
     */
    public function tempFileSave(string $fileContent): void
    {
        try {

            file_put_contents($this->getExecDir().'/'.self::REPORT_FILE_PATH,$fileContent);
//            $varTmpDirectory = $this->filesystem->getDirectoryWrite( $this->getExecDir() );

//            $varTmpDirectory->writeFile(self::REPORT_FILE_PATH, $fileContent);
//            $file = $varTmpDirectory->openFile(self::REPORT_FILE_PATH, 'w');
//            try {
//                $file->lock();
//                try {
//                    $file->write($fileContent);
//                }
//                finally {
//                    $file->unlock();
//                }
//            }
//            finally {
//                $file->close();
//            }

        } catch (FileSystemException $e) {
            $this->logger->error($e->getMessage());
        }
    }

    /**
     * Get Path to csv
     *
     * @param string $csvName
     * @return string|void
     */
    public function getPathToCsvFile(string $csvName)
    {
        try {
            $varDirectoryPath = $this->directoryList->getPath(DirectoryList::VAR_DIR);

            return $varDirectoryPath . '/exec/' . $csvName;
        } catch (FileSystemException $e) {
            $this->logger->error($e->getMessage());
        }
    }

    public function getExecDir()
    {
        return $this->directoryList->getPath(DirectoryList::VAR_DIR) . '/exec';
    }

    public function getVarDir()
    {
        return $this->directoryList->getPath(DirectoryList::VAR_DIR) ;
    }

    /**
     * Convert file
     *
     * @return void
     */
    public function convertFile(): void
    {
        try {
            $tmpFile = $this->getPathToCsvFile(self::NOT_ENCODED_FILE_NAME);
            $varTmpDirectory = $this->filesystem->getDirectoryWrite( $this->getExecDir() );
            if ($this->fileDriver->isExists($tmpFile)) {
                $varTmpDirectory->readFile($tmpFile);
                $this->performEncoding();
            }
        } catch (FileSystemException $e) {
            $this->logger->error($e->getMessage());
        }
    }

    /**
     * Exec
     *
     * @return void
     */
    protected function performEncoding(): void
    {
        $execDirectoryPath = $this->getExecDir();

        $appInitialization = './konw ';
        chdir($execDirectoryPath);
        exec($appInitialization . self::NOT_ENCODED_FILE_NAME . ' '
            . LegacyFormats::ISO_LATIN_1->formatSymbol() . ' '
            . self::ENCODED_FILE_NAME . ' ' . LegacyFormats::MAZOVIA->formatSymbol());
    }


    /**
     * Remove invoice file
     *
     * @param string $filePath
     * @return void
     * @throws FileSystemException
     */
    public function remove(string $filePath)
    {
        if ($this->file->isExists($filePath)) {
            $this->file->deleteFile($filePath);
        }
    }
}

